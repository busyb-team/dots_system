## Notes
1. Our implementation uses any number of robots. The `run_5_busyb.launch.py` was used to test that the algorithm works
1. The implementation builds on the `carry.py` algorithm. Instead of delaying wander by 50 ticks, we included a probability of robots making a turn. Our strategy then increases or reduces this probability based on local environment.
1. We use robot tags to help robots increase their probability of moving away from other robots. So enable tags using the `use_rtags:=true` command line argument.
1. We discovered too late that the bottom of the tables have tag id of 200. We were hoping the ID at the bottom matched that of the 4 tags used to align to the table.
1. We implemented ordered pickup, but for some reason we were unable to see the desired carrier order entered from the commandline. The `/order` topic we subscribed to always published an empty list. So we could not test the ordered pickup for correctness.


### SAMPLE COMMAND TO RUN SIMULATION

`ros2 launch dots_example_controller run_5_busyb.launch.py use_gzclient:=true use_scorer:=true use_rtags:=true`